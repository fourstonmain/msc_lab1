package com.taxi.taxipark.service_client.data;

/**
 * Created by fours on 05.06.2018.
 */
public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
