package com.taxi.taxipark.service_client.data.request;

/**
 * Created by fours on 06.06.2018.
 */
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class UserCreation {
    public String login;
    public String password;
    public String email;
    public String name;
}
