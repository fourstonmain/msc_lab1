package com.taxi.taxipark.user.entity;

/**
 * Created by fours on 05.06.2018.
 */
import com.taxi.taxipark.service_client.data.User;
import lombok.Data;
import com.taxi.taxipark.service_client.data.UserRole;
import com.taxi.taxipark.user.table.UserTable;

import javax.persistence.*;


@Data
@Entity
@Table(name = UserTable.TABLE_NAME)
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = UserTable.LOGIN, nullable = false, unique = true, length = 45)
    private String login;

    @Column(name = UserTable.PASSWORD, nullable = false, length = 60)
    private String password;

    @Column(name = UserTable.GROUP, nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole group;

    @Column(name = UserTable.EMAIL, nullable = false, length = 60)
    private String email;

    @Column(name = UserTable.NAME, nullable = false)
    private String name;

    public User toDto() {
        return new User(id, login, group, email, name);
   }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getGroup() {
        return group;
    }

    public void setGroup(UserRole group) {
        this.group = group;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
